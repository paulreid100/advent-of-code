package Solutions.Y2020

import java.util.*

fun main() {
    print("Enter your input: ")
    val scanner = Scanner(System.`in`).useDelimiter("\n\n")
    val groupAnswers = mutableListOf<String>()
    while (scanner.hasNext()) {
        groupAnswers.add(scanner.next())
    }
    println("Part 1: ${groupAnswers.map { it.replace("\n", "").toSet() }.sumOf { it.size }}")
    println(
        "Part 2: ${
            groupAnswers.map { it.groupBy { char -> char } }
                .map { charMap -> charMap.filter { c -> c.value.size == (charMap['\n']?.size ?: 0) + 1 } }
                .sumOf { it.size }
        }"
    )
}
