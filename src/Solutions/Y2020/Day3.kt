package Solutions.Y2020

import java.util.*

val treeGrid = arrayListOf<CharArray>()

fun main() {
    print("Enter your input: ")
    val scanner = Scanner(System.`in`).useDelimiter("\n")
    while (scanner.hasNext()) {
        treeGrid.add(scanner.next().toCharArray())
    }

    println("Part 1: ${countTrees(3, 1)}")
    println("Part 2: ${countTrees(1, 1) * countTrees(3, 1) * countTrees(5, 1) * countTrees(7, 1) * countTrees(1, 2)}")

}

fun countTrees(moveRight: Int, moveDown: Int): Long {
    val gridWidth = treeGrid[0].size
    var travelRightNumber = 0
    var treeCount = 0L
    for (i in 0 until treeGrid.size step moveDown) {
        if (treeGrid[i][travelRightNumber % gridWidth] == '#')
            treeCount++
        travelRightNumber += moveRight
    }
    return treeCount
}