package Solutions.Y2020

import java.util.*

fun main() {
    print("Enter your input: ")
    val scanner = Scanner(System.`in`).useDelimiter("\n")
    val policiesAndPasswords = mutableListOf<String>()
    while (scanner.hasNext()) {
        policiesAndPasswords.add(scanner.next())
    }

    val extractionPattern = Regex("(\\d+)-(\\d+) (\\w): (\\w+)")
    val part1 = policiesAndPasswords.sumOf {
        val (minOccurrences, maxOccurrences, letter, password) = extractionPattern.find(it)!!.destructured
        if (password.count { char -> char == letter[0] } in minOccurrences.toInt()..maxOccurrences.toInt()) 1 as Int else 0
    }

    val part2 = policiesAndPasswords.sumOf {
        val (firstPosition, secondPosition, letter, password) = extractionPattern.find(it)!!.destructured
        if ((password[firstPosition.toInt() - 1] == letter[0]) xor (password[secondPosition.toInt() - 1] == letter[0])) 1 as Int else 0
    }
    println("Part 1: $part1")
    println("Part 2: $part2")
}