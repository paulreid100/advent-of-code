package Solutions.Y2020

import java.util.*

fun main() {
    print("Enter your input: ")
    val scanner = Scanner(System.`in`).useDelimiter("\n")
    val boardingPasses = mutableListOf<String>()
    while (scanner.hasNext()) {
        boardingPasses.add(scanner.next())
    }

    val seatIds = boardingPasses.map { findSeatId(it) }
    println("Part 1: ${seatIds.maxOrNull()}")
    println("Part 2: ${(seatIds.minOrNull()!!..seatIds.maxOrNull()!!).dropWhile { it in seatIds }.first()}")
}

fun findSeatId(boardingPass: String): Int =
    boardingPass.fold(0) { acc, character -> 2 * acc + if (character == 'B' || character == 'R') 1 else 0 }