package Solutions.Y2020

import java.util.*

val heightPattern = Regex("(\\d+)(cm|in)")
val colourPattern = Regex("#[0-9a-f]{6}")
val eyeColours = listOf("amb", "blu", "brn", "gry", "grn", "hzl", "oth")
val passportIdPattern = Regex("\\d{9}")
val whiteSpacePattern = Regex("\\s")

fun main() {
    print("Enter your input: ")
    val scanner = Scanner(System.`in`).useDelimiter("\n\n")
    val rawPassports = mutableListOf<String>()
    while (scanner.hasNext()) {
        rawPassports.add(scanner.next())
    }
    val necessaryPassportFields = listOf("byr:", "iyr:", "eyr:", "hgt:", "hcl:", "ecl:", "pid:")
    println("Part 1: ${rawPassports.sumOf { passport -> if (necessaryPassportFields.all { passport.contains(it) }) 1 as Int else 0 }}")
    println("Part 1: ${rawPassports.sumOf { passport: String ->
        if (necessaryPassportFields.all {
                passport.contains(it) && hasValidPassportData(
                    passport
                )
            }) 1 as Int else 0
    }
    }")
}

fun hasValidPassportData(passportData: String): Boolean {
    passportData.split(whiteSpacePattern).forEach { fieldAndValue ->
        fieldAndValue.split(":").let { splitData ->
            when (splitData[0]) {
                "byr" -> if (splitData[1].toInt() !in 1920..2002) return false
                "iyr" -> if (splitData[1].toInt() !in 2010..2020) return false
                "eyr" -> if (splitData[1].toInt() !in 2020..2030) return false
                "hgt" -> {
                    if (!heightPattern.matches(splitData[1])) return false
                    val (height, units) = heightPattern.find(splitData[1])!!.destructured
                    when (units) {
                        "in" -> if (height.toInt() !in 59..76) return false
                        "cm" -> if (height.toInt() !in 150..193) return false
                    }
                }
                "hcl" -> if (!colourPattern.matches(splitData[1])) return false
                "ecl" -> if (eyeColours.none { eyeColours.contains(splitData[1]) }) return false
                "pid" -> if (!passportIdPattern.matches(splitData[1])) return false
            }
        }
    }
    return true
}