package Solutions.Y2020

import java.util.*

fun main() {
    print("Enter your input: ")
    val scanner = Scanner(System.`in`).useDelimiter("\n")
    val expenseEntries = mutableSetOf<Int>()
    while (scanner.hasNext()) {
        expenseEntries.add(scanner.next().toInt())
    }

    run part1@{
        expenseEntries.forEach {
            if (expenseEntries.contains(2020 - it)) {
                println("Part 1: ${it * (2020 - it)}")
                return@part1
            }
        }
    }

    run part2@{
        expenseEntries.forEach { first ->
            val filteredEntries = expenseEntries.filter { (2020 - first - it) > 0 }
            filteredEntries.forEach {
                if (filteredEntries.contains(2020 - first - it)) {
                    println("Part 2: ${first * it * (2020 - first - it)}")
                    return@part2
                }
            }
        }
    }
}