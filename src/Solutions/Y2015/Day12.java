package Solutions.Y2015;


import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day12 {
    public static void main(String[] args) {
        System.out.print("Enter your input: ");
        Scanner scanner = new Scanner(System.in);
        String input = "";
        int sum = 0;
        int redSum = 0;
        ArrayList<String> redItems = new ArrayList<>();

        if (scanner.hasNext()) {
            input = scanner.next();
        }

        Pattern numbers = Pattern.compile("-?\\d+");
        Pattern redObjects = Pattern.compile("\\{.*?\"red\".*?}");
        Matcher numberMatcher = numbers.matcher(input);
        Matcher redObjectMatcher = redObjects.matcher(input);
        while (numberMatcher.find()) {
            sum += Integer.parseInt(numberMatcher.group());
        }
        while (redObjectMatcher.find()) {
            redItems.add(redObjectMatcher.group());
            Matcher redNumberMatcher = numbers.matcher(redObjectMatcher.group());
            while (redNumberMatcher.find()) {
                redSum += Integer.parseInt(redNumberMatcher.group());
            }
        }

        System.out.println("The sum of all the numbers is: " + sum);
        System.out.println("The sum of all numbers excluding red items is: " + (sum - redSum));
    }
}
