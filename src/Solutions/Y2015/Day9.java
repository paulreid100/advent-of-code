package Solutions.Y2015;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Day9 {

    private static final Set<String> allNodes = new HashSet<>();
    private static final List<Path> allPaths = new ArrayList<>();

    public static void main(String[] args) {
        System.out.print("Enter your input: ");
        Scanner scanner = new Scanner(System.in).useDelimiter("\n");
        int lowestCost = Integer.MAX_VALUE;
        int highestCost = 0;

        while (scanner.hasNext()) {
            Pattern pattern = Pattern.compile("(\\w+) to (\\w+) = (\\d+)");
            Matcher matcher = pattern.matcher(scanner.next());
            matcher.find();

            allPaths.add(new Path(Integer.valueOf(matcher.group(3)), matcher.group(1), matcher.group(2)));
            allNodes.add(matcher.group(1));
            allNodes.add(matcher.group(2));
        }

        for (String node : allNodes) {
            LinkedHashSet<String> visitedNodes = new LinkedHashSet<>();

            int minTourCost = calculateCost(node, visitedNodes, true);
            if (minTourCost < lowestCost) {
                lowestCost = minTourCost;
            }
            int maxTourCost = calculateCost(node, visitedNodes, false);
            if (maxTourCost > highestCost) {
                highestCost = maxTourCost;
            }
        }
        System.out.println("The shortest route is " + lowestCost);
        System.out.println("The longest route is " + highestCost);
    }

    private static int calculateCost(String startNode, Set<String> visitedNodes, boolean minCost) {
        LinkedHashSet<String> visitedSoFar = new LinkedHashSet<>(visitedNodes);
        visitedSoFar.add(startNode);
        if (visitedSoFar.equals(allNodes)) {
            return 0;
        }
        int minRouteCost = Integer.MAX_VALUE;
        int maxRouteCost = 0;
        List<Path> potentialPaths = allPaths.stream()
                .filter(p -> p.containsEndPoint(startNode))
                .filter(p -> !visitedNodes.contains(p.getDestination(startNode)))
                .collect(Collectors.toList());

        for (Path path : potentialPaths) {
            int routeCost = (path.cost +
                    calculateCost(path.getDestination(startNode), visitedSoFar, minCost));
            if (routeCost < minRouteCost) {
                minRouteCost = routeCost;
            }
            if (routeCost > maxRouteCost) {
                maxRouteCost = routeCost;
            }
        }
        return minCost ? minRouteCost : maxRouteCost;
    }

    private static class Path {
        final String endpoint1;
        final String endpoint2;
        final int cost;

        Path(int cost, String endpoint1, String endpoint2) {
            this.cost = cost;
            this.endpoint1 = endpoint1;
            this.endpoint2 = endpoint2;
        }

        boolean containsEndPoint(String endPoint) {
            return endPoint.equals(endpoint1) || endPoint.equals(endpoint2);
        }

        String getDestination(String startPoint) {
            if (startPoint.equals(this.endpoint1)) {
                return this.endpoint2;
            }
            if (startPoint.equals(this.endpoint2)) {
                return this.endpoint1;
            }
            return "Node not on Path";
        }
    }
}