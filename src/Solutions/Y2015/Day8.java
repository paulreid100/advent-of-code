package Solutions.Y2015;

import java.util.Scanner;

public class Day8 {

    public static void main(String[] args) {
        //Press enter after pasting the input and then press ctrl-D on Linux or ctrl-Z on Windows to signify EOF.
        System.out.print("Enter your input: ");
        Scanner scanner = new Scanner(System.in).useDelimiter("\n");
        int codeNumber = 0;
        int memoryNumber = 0;
        int reencodedNumber = 0;

        while (scanner.hasNext()) {
            String string = scanner.next();
            codeNumber += string.length();
            memoryNumber += string.replaceAll("\\\\x[0-9a-f]{2}", "z")
                    .replaceAll("\\\\[\\\\\"]", "z").length() - 2;
            reencodedNumber += string.replaceAll("(\\\\x[0-9a-f]{2})", "zzzzz")
                    .replaceAll("\\\\[\\\\\"]", "zzzz").length() + 4;
        }
        System.out.println("The number for Part 1 is: " + (codeNumber - memoryNumber));
        System.out.println("The number for Part 2 is: " + (reencodedNumber - codeNumber));
    }
}
