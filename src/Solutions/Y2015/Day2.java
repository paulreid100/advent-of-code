package Solutions.Y2015;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Day2 {

    public static void main(String[] args) {
        //Press enter after pasting the input and then press ctrl-D on Linux or ctrl-Z on Windows to signify EOF.
        System.out.print("Enter your input: ");
        Scanner scanner = new Scanner(System.in);

        int totalArea = 0;
        int totalRibbon = 0;
        while (scanner.hasNext()) {
            String[] dimensions = scanner.next().split("x");
            Box box = new Box(Integer.parseInt(dimensions[0]), Integer.parseInt(dimensions[1]), Integer.parseInt(dimensions[2]));
            totalArea += box.getSurfaceArea() + box.getSmallestSideArea();
            totalRibbon += box.getPerimeterOfShortestFace() + box.getVolume();
        }

        System.out.println("The total area required is: " + totalArea + " and the amount of ribbon required is " + totalRibbon);
    }

    private static class Box {
        final int length;
        final int width;
        final int height;

        Box(int length, int width, int height) {
            this.length = length;
            this.width = width;
            this.height = height;
        }

        int getSurfaceArea() {
            return 2 * length * width + 2 * width * height + 2 * height * length;
        }

        int getSmallestSideArea() {
            int[] measurements = getSmallestTwoSides();
            return measurements[0] * measurements[1];
        }

        int[] getSmallestTwoSides() {
            List<Integer> measurements = Arrays.asList(length, width, height);
            Collections.sort(measurements);
            return new int[]{measurements.get(0), measurements.get(1)};
        }

        int getPerimeterOfShortestFace() {
            int[] smallestMeasurements = getSmallestTwoSides();
            return 2 * smallestMeasurements[0] + 2 * smallestMeasurements[1];
        }

        int getVolume() {
            return length * width * height;
        }
    }
}