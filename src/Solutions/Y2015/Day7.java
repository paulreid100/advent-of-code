package Solutions.Y2015;

import java.util.HashMap;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day7 {

    public static void main(String[] args) {
        //Press enter after pasting the input and then press ctrl-D on Linux or ctrl-Z on Windows to signify EOF.
        System.out.print("Enter your input: ");
        Scanner scanner = new Scanner(System.in).useDelimiter("\n");
        HashMap<String, String> circuit = new HashMap<>();

        while (scanner.hasNext()) {
            String[] component = scanner.next().split("->");
            circuit.put(component[1].trim(), component[0].trim());
        }
        HashMap<String, String> circuitPart2 = new HashMap<>(circuit);
        //Now that the circuit is complete, recursively calculate the output of the given wire
        int aValue = getValue("a", circuit);
        System.out.println("The value of a is: " + aValue);
        circuitPart2.put("b", String.valueOf(aValue));
        System.out.println("The value of a for Part 2 is: " + getValue("a", circuitPart2));
    }

    public static int getValue(String label, HashMap<String, String> circuit) {
        String wireInput = circuit.get(label);
        int output;

        if (wireInput == null)
            return Integer.valueOf(label); //raw signal input if we have no component in the circuit with this label

        Pattern pattern = Pattern.compile("^(((\\w+ )?(AND|OR|([LR])SHIFT|NOT) (\\w+))|(\\d+)|(\\w+))");
        Matcher matcher = pattern.matcher(wireInput);
        matcher.find();
        String logicGate = matcher.group(4);
        String signal = matcher.group(7);

        if (logicGate == null)
            if (signal == null) {
                //We're looking at a wire connected to another wire
                output = (getValue(matcher.group(1), circuit) & 0x0000FFFF);
                circuit.put(label, String.valueOf(output));
                return output;
            } else
                //Direct signal!
                return (Integer.valueOf(matcher.group(1)) & 0x0000FFFF);
        else {
            switch (logicGate) {
                case "AND":
                    output = (getValue(matcher.group(3).trim(), circuit)
                            & getValue(matcher.group(6), circuit)) & 0x0000FFFF;
                    circuit.put(label, String.valueOf(output));
                    return output;
                case "OR":
                    output = (getValue(matcher.group(3).trim(), circuit)
                            | getValue(matcher.group(6), circuit)) & 0x0000FFFF;
                    circuit.put(label, String.valueOf(output));
                    return output;
                case "LSHIFT":
                    output = (getValue(matcher.group(3).trim(), circuit)
                            << Integer.valueOf(matcher.group(6))) & 0x0000FFFF;
                    circuit.put(label, String.valueOf(output));
                    return output;
                case "RSHIFT":
                    output = getValue(matcher.group(3).trim(), circuit)
                            >>> Integer.valueOf(matcher.group(6));
                    circuit.put(label, String.valueOf(output));
                    return output;
                case "NOT":
                    output = ~(getValue(matcher.group(6), circuit) | 0xFFFF0000);
                    circuit.put(label, String.valueOf(~(getValue(matcher.group(6), circuit) | 0xFFFF0000)));
                    return output;
            }
        }
        return -1;
    }
}