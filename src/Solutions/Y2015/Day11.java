package Solutions.Y2015;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day11 {

    public static void main(String[] args) {
        String input = "";
        System.out.print("Enter your input: ");
        Scanner scanner = new Scanner(System.in);

        if (scanner.hasNext()) {
            input = scanner.next();
        }

        String nextPassword = input;
        do {
            nextPassword = countUp(nextPassword);
        }
        while (!(includesStraightOfThree(nextPassword)
                && doesNotContainConfusingLetters(nextPassword)
                && containsTwoPairsOfLetters(nextPassword)));

        System.out.println("Santa's next password will be: " + nextPassword);
    }

    private static String countUp(String password) {
        StringBuilder sb = new StringBuilder();
        boolean carryOver = true;
        for (int i = password.length() - 1; i >= 0; i--) {
            int currentChar = Character.getNumericValue(password.charAt(i));

            if (carryOver) {
                currentChar++;
                if (currentChar == 36) {
                    currentChar = 10;
                    carryOver = true;
                } else {
                    carryOver = false;
                }
            }
            sb.append(Character.forDigit(currentChar, Character.MAX_RADIX));
            if (i == 0 && currentChar == 10 && carryOver) {
                sb.append('a');
            }
        }
        return sb.reverse().toString();
    }

    private static boolean includesStraightOfThree(String password) {
        for (int i = 0; i < password.length(); i++) {
            if (i + 2 > password.length() - 1) {
                return false;
            }
            int first = Character.getNumericValue(password.charAt(i));
            int second = Character.getNumericValue(password.charAt(i + 1));
            int third = Character.getNumericValue(password.charAt(i + 2));

            if ((second - first == 1) && (third - second == 1)) {
                return true;
            }
        }
        return false;
    }

    private static boolean doesNotContainConfusingLetters(String password) {
        return !(password.contains("i") || password.contains("o") || password.contains("l"));
    }

    private static boolean containsTwoPairsOfLetters(String password) {
        Pattern pattern = Pattern.compile("(\\w)\\1\\w*((?!\\1)\\w)\\2");
        Matcher matcher = pattern.matcher(password);
        return matcher.find();
    }
}
