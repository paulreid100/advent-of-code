package Solutions.Y2015;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day6 {
    public static void main(String[] args) {
        //Press enter after pasting the input and then press ctrl-D on Linux or ctrl-Z on Windows to signify EOF.
        System.out.print("Enter your input: ");
        Scanner scanner = new Scanner(System.in).useDelimiter("\n");
        Light[][] lightGrid = new Light[1000][1000];
        int lightsLit = 0;
        int totalBrightness = 0;

        while (scanner.hasNext()) {
            Command command = parseCommand(scanner.next());
            int[] lightsDiff = command.apply(lightGrid);
            lightsLit += lightsDiff[0];
            totalBrightness += lightsDiff[1];
        }
        System.out.println("Number of lights lit is: " + lightsLit);
        System.out.println("Total brightness is: " + totalBrightness);

    }

    private static Command parseCommand(String command) {
        Pattern pattern = Pattern.compile("^(turn (on|off)|toggle) (\\d+,\\d+) through (\\d+,\\d+)");
        Matcher matcher = pattern.matcher(command);
        matcher.find();
        String startPoint = matcher.group(3);
        String endPoint = matcher.group(4);
        return new Command(matcher.group(1), Integer.parseInt(startPoint.split(",")[0]),
                Integer.parseInt(startPoint.split(",")[1]), Integer.parseInt(endPoint.split(",")[0]),
                Integer.parseInt(endPoint.split(",")[1]));
    }

    private static class Command {
        final String action;
        final int fromX;
        final int fromY;
        final int toX;
        final int toY;

        Command(String action, int fromX, int fromY, int toX, int toY) {
            this.action = action;
            this.fromX = fromX;
            this.fromY = fromY;
            this.toX = toX;
            this.toY = toY;
        }

        int[] apply(Light[][] grid) {
            int lightsLit = 0;
            int brightnessAdded = 0;
            for (int i = fromX; i <= toX; i++) {
                for (int j = fromY; j <= toY; j++) {
                    if (grid[i][j] == null)
                        grid[i][j] = new Light();
                    switch (this.action.toLowerCase()) {
                        case "turn on":
                            if (!grid[i][j].switchedOn)
                                lightsLit++;
                            grid[i][j].switchedOn = true;
                            brightnessAdded += grid[i][j].increaseBrightness(1);
                            break;
                        case "turn off":
                            if (grid[i][j].switchedOn)
                                lightsLit--;
                            grid[i][j].switchedOn = false;
                            brightnessAdded += grid[i][j].lowerBrightness();
                            break;
                        case "toggle":
                            grid[i][j].switchedOn = !grid[i][j].switchedOn;
                            if (grid[i][j].switchedOn)
                                lightsLit++;
                            else
                                lightsLit--;
                            brightnessAdded += grid[i][j].increaseBrightness(2);
                            break;
                    }
                }
            }
            return new int[]{lightsLit, brightnessAdded};
        }
    }

    private static class Light {
        boolean switchedOn;
        int brightness;

        Light() {
            this.switchedOn = false;
            this.brightness = 0;
        }

        int lowerBrightness() {
            if (brightness > 0) {
                brightness -= 1;
                return -1;
            }
            return 0;
        }

        int increaseBrightness(int amount) {
            this.brightness += amount;
            return amount;
        }
    }
}

