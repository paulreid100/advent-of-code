package Solutions.Y2017

import kotlin.math.abs
import kotlin.math.max

fun main(args: Array<String>) {
    print("Enter your input: ")
    val input = (readLine() ?: "").split(",")
    val finalLocation = HexLocation(0.0, 0.0, 0.0)
    var maxDistance = 0.0
    input.forEach {
        when (it) {
            "n"  -> { finalLocation.y++; finalLocation.z-- }
            "ne" -> { finalLocation.x++; finalLocation.z-- }
            "se" -> { finalLocation.x++; finalLocation.y-- }
            "s"  -> { finalLocation.y--; finalLocation.z++ }
            "sw" -> { finalLocation.x--; finalLocation.z++ }
            "nw" -> { finalLocation.x--; finalLocation.y++ }
        }
        maxDistance = max(maxDistance, getDistance(HexLocation(0.0, 0.0, 0.0),finalLocation))
    }
    println(getDistance(HexLocation(0.0, 0.0, 0.0), finalLocation))
    println(maxDistance)
}

fun getDistance(start: HexLocation, end: HexLocation) =
        max(max(abs(start.x - end.x), abs(start.y - end.y)), abs(start.z - end.z))

data class HexLocation(var x: Double, var y: Double, var z: Double)