package Solutions.Y2017

fun main(args: Array<String>) {
    print("Enter your input: ")
    val input = readLine() ?: ""
    var openingBraces = 0
    var score = 0
    var inGarbage = false
    var ignoreNext = false
    var garbageCount = 0
    for (char in input) {
        if (ignoreNext) {
            ignoreNext = false
            continue
        }
        when (char) {
            '!' -> {
                ignoreNext = true
                if (inGarbage) garbageCount--
            }
            '{' -> if (!inGarbage) openingBraces++
            '}' -> if (!inGarbage) {
                score += openingBraces
                openingBraces--
            }
            '<' -> {
                if (!inGarbage) garbageCount--
                inGarbage = true
            }
            '>' -> inGarbage = false
        }
        if (inGarbage)
            garbageCount++
    }
    println(score)
    println(garbageCount)
}