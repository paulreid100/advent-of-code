package Solutions.Y2017

fun main(args: Array<String>) {
    //Press enter after pasting the input and then press ctrl-D on Linux or ctrl-Z on Windows to signify EOF
    print("Enter your input: ")
    val inputLine = readLine() ?: ""
    println(calculateSum(inputLine, 1))
    println(calculateSum(inputLine, inputLine.length / 2))
}

fun calculateSum(input: String, steps: Int): Int {
    val inputToProcess = input + input.subSequence(0 until steps)
    return (0 until inputToProcess.length - steps)
        .filter { inputToProcess[it] == inputToProcess[it + steps] }
        .sumOf { inputToProcess[it].toString().toInt() }
}