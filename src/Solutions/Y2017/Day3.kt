@file:Suppress("EXPERIMENTAL_FEATURE_WARNING")

package Solutions.Y2017

import kotlin.math.abs
import kotlin.math.pow

fun main(args: Array<String>) {
    print("Enter your input: ")
    val input = (readLine() ?: "0").toInt()

    val squareLengths = generateSequence(3.0) { it + 2 }.takeWhileInclusive { it.pow(2) <= input }.toList()
    val layerLength = squareLengths.last()
    val lastNumberInLayer = layerLength.pow(2).toInt()
    val layerNumber = squareLengths.size
    val squareSides = generateSequence(lastNumberInLayer downTo (lastNumberInLayer - (layerLength - 1)).toInt())
    { it.last downTo (it.last - (layerLength - 1)).toInt() }.take(4).toList()

    val sideWithInput = squareSides.first { it.contains(input) }
    println(layerNumber + abs(sideWithInput.indexOf(input) - layerNumber))

    //use generator to generate coordinates and generate a value by looking for populated values in all directions and summing them!
    println(populateSpiral(input))
}

fun <T> Sequence<T>.takeWhileInclusive(predicate: (T) -> Boolean): Sequence<T> {
    var shouldContinue = true
    return takeWhile {
        val result = shouldContinue
        shouldContinue = predicate(it)
        result
    }
}

fun populateSpiral(input: Int): Int {
    val grid = mutableMapOf<Pair<Int, Int>, Int>()
    return generateDeathSpiralCoordinates()
        .dropWhile { populateGrid(grid, it) <= input }
        .first()
        .run { grid.getOrDefault(this, 0) }
}

fun populateGrid(grid: MutableMap<Pair<Int, Int>, Int>, coordinates: Pair<Int, Int>): Int {
    val directions =
        listOf(Pair(1, 0), Pair(1, 1), Pair(0, 1), Pair(-1, 1), Pair(-1, 0), Pair(-1, -1), Pair(0, -1), Pair(1, -1))
    grid[coordinates] = when (coordinates) {
        Pair(0, 0) -> 1
        else -> directions.map { (x, y) -> Pair(coordinates.first + x, coordinates.second + y) }
            .map { grid.getOrDefault(it, 0) }.sum()
    }
    return grid.getOrDefault(coordinates, 0)
}

fun generateDeathSpiralCoordinates(): Sequence<Pair<Int, Int>> = sequence {
    // generates all the spiral coordinates, rotated by -90 degrees but doesn't matter!
    var x = 0
    var y = 0
    var dx = 0
    var dy = 1

    while (true) {
        yield(Pair(x, y))
        if ((abs(x) == abs(y) && (x < 0 || y < 0)) || (x >= 0 && (y - x) == 1)) {
            val tmp = dx
            dx = -dy
            dy = tmp
        }
        x += dx
        y += dy
    }
}

