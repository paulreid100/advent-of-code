package Solutions.Y2017

import java.util.*

fun main(args: Array<String>) {
    print("Enter your input: ")
    val scanner = Scanner(System.`in`).useDelimiter("\n")
    val rowList = mutableListOf<String>()
    while (scanner.hasNext()) {
        rowList.add(scanner.next())
    }

    println(countValidPasswords(rowList) { it == it.distinct() })
    println(countValidPasswords(rowList) { it ->
        it == it.distinct()
                && it.map { it.toSortedSet() }.distinct().size == it.distinct().size
    })
}

fun countValidPasswords(rowList: List<String>, isValid: (List<String>) -> Boolean): Int {
    return rowList
        .map { it.split(" ") }
        .count { isValid(it) }
}