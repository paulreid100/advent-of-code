package Solutions.Y2017

fun main(args: Array<String>) {
    print("Enter your input: ")
    val input = (readLine() ?: "0")
    val memoryBanks = input.split("\t").map { it.toInt() }.toMutableList()

    val observedStates = mutableSetOf<List<Int>>()
    var bankWithMostBlocks = memoryBanks.indexOf(memoryBanks.maxOrNull())
    var redistributionCycles = 0
    var firstDuplicateCycleNumber = 0
    var duplicatesEncountered = 0
    var firstDuplicate = listOf<Int>()
    while (true) {
        val blocksToRedistribute = memoryBanks[bankWithMostBlocks]
        memoryBanks[bankWithMostBlocks] = 0
        var currentBlock = if (bankWithMostBlocks + 1 == memoryBanks.size) 0 else bankWithMostBlocks + 1
        for (i in 1..blocksToRedistribute) {
            memoryBanks[currentBlock] += 1
            currentBlock++
            if (currentBlock == memoryBanks.size)
                currentBlock = 0
        }
        redistributionCycles++
        if (!observedStates.add(memoryBanks)) {
            duplicatesEncountered++
            if (duplicatesEncountered == 1) {
                firstDuplicate = memoryBanks.toList()
                firstDuplicateCycleNumber = redistributionCycles
                println(firstDuplicateCycleNumber)
            }
            if (duplicatesEncountered > 1 && memoryBanks == firstDuplicate) {
                println(redistributionCycles - firstDuplicateCycleNumber)
                break
            }
        }
        bankWithMostBlocks = memoryBanks.indexOf(memoryBanks.maxOrNull())
    }
}