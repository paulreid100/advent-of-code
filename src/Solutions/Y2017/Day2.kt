package Solutions.Y2017

import java.util.*

fun main(args: Array<String>) {
    print("Enter your input: ")
    val scanner = Scanner(System.`in`).useDelimiter("\n")
    val rowList = mutableListOf<String>()
    while (scanner.hasNext()) {
        rowList.add(scanner.next())
    }
    println(calculateChecksum(rowList) { it.maxOrNull()!! - it.minOrNull()!! })
    println(calculateChecksum(rowList, ::evenlyDivides))
}

fun evenlyDivides(row: List<Int>): Int {
    for (i in 0 until row.count()) {
        (0 until row.count())
            .filter { row[i] % row[it] == 0 && i != it }
            .forEach { return row[i] / row[it] }
    }
    return 0
}

fun calculateChecksum(rowList: List<String>, rowValue: (List<Int>) -> Int): Int {
    return rowList
        .map { it.split("\t") }.sumOf { rowItem ->
            rowValue(rowItem.map { it.toInt() })
        }
}
