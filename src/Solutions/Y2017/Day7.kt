package Solutions.Y2017

import java.util.*
import java.util.regex.Pattern

fun main(args: Array<String>) {
    print("Enter your input: ")
    val scanner = Scanner(System.`in`).useDelimiter("\n")
    val tree = mutableSetOf<Program>()

    while (scanner.hasNext()) {
        val row = scanner.next().split("->")
        val pattern = Pattern.compile("^(\\w+) \\((\\d+)\\)")
        val matcher = pattern.matcher(row[0])
        if (matcher.find()) {
            val programName = matcher.group(1)
            val weight = matcher.group(2).toInt()
            val programToAdd = Program(programName, weight)
            val existingNode = tree.firstOrNull { it.name == programName }
            if (existingNode == null)
                tree.add(programToAdd)
            else
                existingNode.weight = weight
            if (row.size > 1) {
                val programs = row[1].split(", ")
                    .map { rowEntry ->
                        tree.find { program -> program.name == rowEntry.trim() } ?: Program(rowEntry.trim(), weight)
                    }
                tree.addAll(programs)
                programs.forEach { it.parent = existingNode ?: programToAdd }
            }
        }
    }
    val rootNode = tree.first { it.parent == null }
    println(rootNode)
    println(tree.findMismatchingWeight(tree.getDeepestUnbalancedPlate()))
}

fun Set<Program>.getDeepestUnbalancedPlate(): Program =
    filter { getChildren(it).size > 1 && getChildrenWeightDistribution(it).size > 1 }.sortedByDescending { getDepth(it) }.first()

fun Set<Program>.getDepth(node: Program?): Int = if (node == null) 0 else 1 + this.getDepth(node.parent)

fun Set<Program>.getChildren(node: Program): List<Program> = filter { it.parent == node }

tailrec fun Set<Program>.findMismatchingWeight(node: Program): Int {
    val weightDistribution = getChildrenWeightDistribution(node)
    val oddOne = weightDistribution.filter { it.value.size == 1 }.entries.first().value.first().first
    return if (!getChildren(oddOne).any() || getChildrenWeightDistribution(oddOne).size == 1)
        oddOne.weight + (weightDistribution.filter { it.value.size > 1 }.keys.first() - calculateWeight(oddOne))
    else
        findMismatchingWeight(oddOne)
}

fun Set<Program>.getChildrenWeightDistribution(node: Program): Map<Int, List<Pair<Program, Int>>> =
    getChildren(node).map { Pair(it, calculateWeight(it)) }.groupBy { it.second }

fun Set<Program>.calculateWeight(parentNode: Program): Int {
    val childNodes = getChildren(parentNode)
    return if (childNodes.isEmpty())
        parentNode.weight
    else
        parentNode.weight + childNodes.map { calculateWeight(it) }.sum()
}

data class Program(val name: String, var weight: Int, var parent: Program? = null)