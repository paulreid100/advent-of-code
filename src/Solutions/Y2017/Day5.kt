package Solutions.Y2017

import java.util.*

fun main(args: Array<String>) {
    print("Enter your input: ")
    val scanner = Scanner(System.`in`).useDelimiter("\n")
    val instructionList = mutableListOf<Int>()
    while (scanner.hasNext()) {
        instructionList.add(scanner.next().toInt())
    }
    println(jump(instructionList.toMutableList()) { it + 1 })
    println(jump(instructionList.toMutableList()) { if (it >= 3) it - 1 else it + 1 })
}

fun jump(instructionList: MutableList<Int>, calculateOffset: (Int) -> Int): Int {
    var instructionIndex = 0
    var steps = 0
    while (instructionIndex < instructionList.size && instructionIndex >= 0) {
        val offSet = instructionList[instructionIndex]
        instructionList[instructionIndex] = calculateOffset(offSet)
        instructionIndex += offSet
        steps++
    }
    return steps
}