package Solutions.Y2017

import java.util.*
import java.util.regex.Pattern
import kotlin.math.max

fun main(args: Array<String>) {
    print("Enter your input: ")
    val scanner = Scanner(System.`in`).useDelimiter("\n")
    val instructionList = mutableListOf<Instruction>()
    val registers = mutableSetOf<String>()
    while (scanner.hasNext()) {
        val line = scanner.next()
        val pattern = Pattern.compile("^(\\w+) (inc|dec) (-?\\d+) if (\\w+) ([><=!]{1,2}) (-?\\d+)")
        val matcher = pattern.matcher(line)
        if (matcher.find()) {
            val targetRegister = matcher.group(1)
            val targetMultiplier = if (matcher.group(2) == "inc") 1 else -1
            val targetChangeValue = matcher.group(3).toInt()
            val conditionRegister = matcher.group(4)
            val conditionOperator = matcher.group(5)
            val conditionValue = matcher.group(6).toInt()
            instructionList.add(
                Instruction(
                    targetRegister,
                    targetMultiplier,
                    targetChangeValue,
                    conditionRegister,
                    conditionOperator,
                    conditionValue
                )
            )
            registers.addAll(listOf(targetRegister, conditionRegister))
        }
    }
    val registerValues = registers.associate { it to 0 }.toMutableMap()
    var maxVal = 0
    instructionList.forEach {
        val operationResult = registerValues.evaluate(it)
        maxVal = max(maxVal, operationResult)
    }
    println(registerValues.values.maxOrNull())
    println(maxVal)
}

fun MutableMap<String, Int>.evaluate(instruction: Instruction): Int {
    if (when (instruction.conditionOperator) {
            "==" -> this[instruction.conditionRegister] == instruction.conditionValue
            "!=" -> this[instruction.conditionRegister] != instruction.conditionValue
            ">=" -> this[instruction.conditionRegister]!! >= instruction.conditionValue
            "<=" -> this[instruction.conditionRegister]!! <= instruction.conditionValue
            ">" -> this[instruction.conditionRegister]!! > instruction.conditionValue
            "<" -> this[instruction.conditionRegister]!! < instruction.conditionValue
            else -> false
        }
    )
        this[instruction.targetRegister] = (instruction.targetChangeValue * instruction.targetMultiplier) +
                this[instruction.targetRegister]!!
    return this[instruction.targetRegister]!!
}

data class Instruction(
    val targetRegister: String,
    val targetMultiplier: Int,
    val targetChangeValue: Int,
    val conditionRegister: String,
    val conditionOperator: String,
    val conditionValue: Int
)