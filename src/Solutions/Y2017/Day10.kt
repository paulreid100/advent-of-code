package Solutions.Y2017

fun main(args: Array<String>) {
    print("Enter your input: ")
    val input = readLine() ?: ""
    val part1Result = sparseHash(input, 1)
    println(part1Result[0] * part1Result[1])
    println(denseHash(sparseHash(toPart2Input(input), 64)).toHex())
}

fun toPart2Input(input: String) = input.map { it.code }.joinToString(",", postfix = ",17,31,73,47,23")

fun sparseHash(input: String, rounds: Int): List<Int> {
    val numberList = (0..255).toMutableList()
    var position = 0
    var skip = 0
    val lengths = input.split(",").map { it.toInt() }
    for (i in 1..rounds) {
        lengths.forEach {
            val intoOverflow = position + it >= numberList.size
            val overFlow = (position + it) % numberList.size
            val subList = if (!intoOverflow) {
                numberList.slice(position until position + it)
            } else {
                (numberList.slice(position until numberList.size) + numberList.slice(0 until overFlow))
            }
            for (j in 0 until it) {
                val index = if (intoOverflow) (position + j) % numberList.size else position + j
                numberList[index] = subList[it - 1 - j]
            }
            position = (position + it + skip) % numberList.size
            skip++
        }
    }
    return numberList
}

fun denseHash(sparseHash: List<Int>) = sparseHash
    .chunked(16)
    .map { it.fold(0) { total, value -> total xor value } }

fun List<Int>.toHex() = this.joinToString("") { it.toString(16).padStart(2, '0') }