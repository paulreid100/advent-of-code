package Solutions.Y2018

import java.util.*
import java.util.regex.Pattern

fun main(args: Array<String>) {
    print("Enter your input: ")
    val scanner = Scanner(System.`in`).useDelimiter("\n")
    val claimedPoints = mutableMapOf<Pair<Int, Int>,Int>()
    val idPoints = mutableMapOf<Int, MutableList<Pair<Int,Int>>>()
    while (scanner.hasNext()) {
        val line = scanner.next()
        val pattern = Pattern.compile("#(\\d+) @ (\\d+),(\\d+): (\\d+)x(\\d+)")
        val matcher = pattern.matcher(line)
        if (matcher.find()) {
            val id = matcher.group(1).toInt()
            val fromLeft = matcher.group(2).toInt()
            val fromTop = matcher.group(3).toInt()
            val width = matcher.group(4).toInt()
            val height = matcher.group(5).toInt()

            idPoints[id] = mutableListOf()
            for (x in fromLeft until fromLeft + width) {
                for (y in fromTop until fromTop + height) {
                    idPoints[id]!!.add(Pair(x, y))
                    claimedPoints[Pair(x, y)] = (claimedPoints[Pair(x, y)] ?: 0) + 1
                }
            }
        }
    }
    println("Number of square inches with multiple claims is ${claimedPoints.filter { it.value > 1 }.size}")
    println("ID of only claim that doesn't overlap is: ${idPoints.filter { rect -> rect.value.all { claimedPoints[it] == 1 } }.keys.first()}")
}