package Solutions.Y2018

import java.time.LocalDateTime
import java.util.*
import java.util.regex.Pattern

fun main(args: Array<String>) {
    print("Enter your input: ")
    val scanner = Scanner(System.`in`).useDelimiter("\n")
    val eventList = mutableListOf<Event>()
    while (scanner.hasNext()) {
        val line = scanner.next()
        val pattern =
            Pattern.compile("\\[(\\d{4})-(\\d{2})-(\\d{2}) (\\d{2}):(\\d{2})] (Guard #(\\d+) begins shift|falls asleep|wakes up)")
        val matcher = pattern.matcher(line)
        if (matcher.find()) {
            val year = matcher.group(1).toInt()
            val month = matcher.group(2).toInt()
            val day = matcher.group(3).toInt()
            val hour = matcher.group(4).toInt()
            val minutes = matcher.group(5).toInt()
            val event = matcher.group(6)
            val guardId = matcher.group(7)?.toInt()

            eventList.add(Event(LocalDateTime.of(year, month, day, hour, minutes), event, guardId))
        }
    }
    eventList.sortBy { it.timeStamp }
    val sleepPeriods = mutableMapOf<Int, MutableList<Int>>()
    var startMinute = 0
    for (index in eventList.indices) {
        if (eventList[index].guardId == null)
            eventList[index].guardId = eventList[index - 1].guardId
        when (eventList[index].event) {
            "falls asleep" -> startMinute = eventList[index].timeStamp.minute
            "wakes up" -> sleepPeriods[eventList[index].guardId]!!.addAll((startMinute until eventList[index].timeStamp.minute))
            else -> sleepPeriods[eventList[index].guardId!!] = sleepPeriods[eventList[index].guardId] ?: mutableListOf()
        }
    }
    val strategyOneSleepyGuardId = sleepPeriods.maxByOrNull { it: Map.Entry<Int, MutableList<Int>> -> it.value.size }!!.key
    val sleepiestMinute =
        sleepPeriods.filter { it.key == strategyOneSleepyGuardId }.values.first().groupBy { it }
            .maxByOrNull { it: Map.Entry<Int, List<Int>> -> it.value.size }!!.key
    println("Part 1 answer is ${strategyOneSleepyGuardId * sleepiestMinute}")

    val strategyTwoSleepyMinute = sleepPeriods.map { period ->
        val mostFrequentlySleptMinute =
            period.value.groupBy { it }.maxByOrNull { it: Map.Entry<Int, List<Int>> -> it.value.size }
        SleepMinute(period.key, mostFrequentlySleptMinute?.key ?: 0, mostFrequentlySleptMinute?.value?.size ?: 0)
    }
        .groupBy { it.minute }
        .map { minuteMap ->
            minuteMap.value.maxByOrNull { it: SleepMinute -> it.occurrences }!!
        }
        .maxByOrNull { it: SleepMinute -> it.occurrences }!!

    println("Part 2 answer: ${strategyTwoSleepyMinute.guardId * strategyTwoSleepyMinute.minute}")
}

data class Event(val timeStamp: LocalDateTime, val event: String, var guardId: Int?)
data class SleepMinute(val guardId: Int, val minute: Int, val occurrences: Int)