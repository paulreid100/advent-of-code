package Solutions.Y2018

import java.util.*

fun main(args: Array<String>) {
    print("Enter your input: ")
    val scanner = Scanner(System.`in`).useDelimiter("\n")
    val boxIds = mutableListOf<String>()
    while (scanner.hasNext()) {
        boxIds.add(scanner.next())
    }

    var twoLetterCount = 0
    var threeLetterCount = 0

    boxIds.forEach { id ->
        val letterCounts = id.groupBy { it }
        if (letterCounts.any { it.value.size == 2 }) twoLetterCount++
        if (letterCounts.any { it.value.size == 3 }) threeLetterCount++
    }
    println("The checksum is ${twoLetterCount * threeLetterCount}")

    for (i in boxIds[0].indices) {
        val matchingLetters =
            boxIds.map { it.removeRange(i..i) }.groupBy { it }.filter { it.value.size == 2 }.keys.firstOrNull()
        if (matchingLetters != null)
            println("The matching letters are $matchingLetters")
    }
}