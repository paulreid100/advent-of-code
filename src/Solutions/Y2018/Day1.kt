package Solutions.Y2018

import java.util.*

fun main(args: Array<String>) {
    print("Enter your input: ")
    val scanner = Scanner(System.`in`).useDelimiter("\n")
    val frequencyChanges = mutableListOf<Long>()
    while (scanner.hasNext()) {
        frequencyChanges.add(scanner.nextLong())
    }

    println("The resulting frequency is ${frequencyChanges.sum()}")
    val frequenciesReached = mutableSetOf<Long>()

    frequencyChanges.repeatIndefinitely().fold(0L) { sumSoFar, nextValue ->
        if (!frequenciesReached.add(sumSoFar)) {
            println("The first frequency reached twice is $sumSoFar")
            return
        }
        sumSoFar + nextValue
    }
}

fun <T> List<T>.repeatIndefinitely(): Sequence<T> =
    generateSequence { this }.flatten()
