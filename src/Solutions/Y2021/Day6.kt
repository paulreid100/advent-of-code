package Solutions.Y2021


fun main() {
    val fishCounts = MutableList(9) { 0L }
    readLine()!!
        .split(",")
        .forEach { fishCounts[it.toInt()]++ }

    println("After 80 days there will be ${getCountAfterDays(fishCounts, 80)}")
    println("After 80 days there will be ${getCountAfterDays(fishCounts, 256)}")

}

fun getCountAfterDays(startState: List<Long>, days: Int): Long {
    val state = ArrayDeque(startState)
    repeat(days) {
        val spawn = state.removeFirst()
        state.add(spawn)
        state[6] += spawn
    }
    return state.sum()
}

