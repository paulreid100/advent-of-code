package Solutions.Y2021

import kotlin.math.abs
import kotlin.math.floor
import kotlin.math.min

fun main() {
    val crabPositions = readLine()!!
        .split(",")
        .map { it.toInt() }

    val medianPos = crabPositions.sorted()
        .let { it[it.size / 2] }
    val meanPos = floor(crabPositions.average())

    println("Part 1: ${crabPositions.sumOf { abs(it - medianPos) }}")
    println("Part 2: ${min(crabPositions.sumOf { abs(it - meanPos) * (abs(it - meanPos) + 1) / 2 }.toLong(),
        crabPositions.sumOf { abs(it - (meanPos+1)) * (abs(it - (meanPos+1)) + 1) / 2 }.toLong())}")
// rounding/flooring of average may have gone in wrong direction
}