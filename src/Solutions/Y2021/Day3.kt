package Solutions.Y2021

import java.util.*

fun main() {
    print("Enter your input: ")
    val scanner = Scanner(System.`in`).useDelimiter("\n")
    val diagnostics = mutableListOf<String>()
    while (scanner.hasNext()) {
        diagnostics.add(scanner.next())
    }

    val sumList = MutableList(diagnostics[0].length) { 0 }

    diagnostics.forEach {
        for (i in it.indices) {
            sumList[i] += it[i].digitToInt()
        }
    }

    val gammaRate = sumList.map { if (it > (diagnostics.size / 2.0)) '1' else '0' }.joinToString("").toInt(2)
    val epsilonRate = gammaRate xor Collections.nCopies(diagnostics[0].length, 1).joinToString("").toInt(2)
    println("The power consumption of the submarine is: ${gammaRate * epsilonRate}")

    val o2Rating = getRating(diagnostics, "", false).toInt(2)
    val co2Rating = getRating(diagnostics, "", true).toInt(2)
    println("The life support rating is: ${o2Rating * co2Rating}")
}

fun getRating(diagnostics: List<String>, prefix: String, invert: Boolean): String {
    if (diagnostics.size == 1)
        return diagnostics[0]
    val mostCommon = diagnostics.sumOf { it[prefix.length].digitToInt() }
    val newPrefix = prefix + if ((mostCommon >= (diagnostics.size / 2.0)) xor invert) "1" else "0"
    return getRating(diagnostics.filter { it.startsWith(newPrefix) }, newPrefix, invert)
}