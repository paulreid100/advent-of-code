package Solutions.Y2021

import java.util.*

data class BingoNumber(val value: Int, var called: Boolean = false)
data class BingoBoard(val rows: List<List<BingoNumber>>, var hasWon: Boolean = false)

fun main() {
    print("Enter your input: ")
    val scanner = Scanner(System.`in`).useDelimiter("\n")
    val bingoInput = mutableListOf<String>()
    while (scanner.hasNext()) {
        bingoInput.add(scanner.next())
    }

    val numberDraw = bingoInput.first().split(',').map { it.toInt() }
    val bingoBoards = bingoInput.drop(1)
        .filter { it.isNotEmpty() }
        .chunked(5)
        .map { board ->
            BingoBoard(board.map { row ->
                row.split(" ")
                    .filter { it.isNotEmpty() }
                    .map { number -> BingoNumber(number.toInt()) }
            })
        }

    val allBingoNumbers = bingoBoards.map { it.rows }.flatten().flatten().groupBy { it.value }

    numberDraw.takeWhile { drawnNumber ->
        allBingoNumbers.filter { it.key == drawnNumber }
            .onEach { group -> group.value.onEach { number -> number.called = true } }

        bingoBoards.filter { !it.hasWon }.forEach { board ->
            if (board.rows.any { it.all { number -> number.called } } || board.rows.first().indices.any { column ->
                    board.rows.map { it[column] }.all { number -> number.called }
                }) {
                board.hasWon = true
                println("Winning board score is: ${
                    board.rows.flatten().filter { !it.called }.sumOf { it.value } * drawnNumber
                }")
            }
        }
        bingoBoards.any { !it.hasWon }
    }
}