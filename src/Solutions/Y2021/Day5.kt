package Solutions.Y2021

import java.util.*
import kotlin.math.abs
import kotlin.math.max

fun main() {
    print("Enter your input: ")
    val scanner = Scanner(System.`in`).useDelimiter("\n")
    val ventInput = mutableListOf<String>()
    while (scanner.hasNext()) {
        ventInput.add(scanner.next())
    }

    val ventPoints = ventInput.map { it.split("->") }
        .map {
            it.map { startAndEndCoordinates -> startAndEndCoordinates.trim().split(",") }
                .map { coordinates -> Pair(coordinates.first().toInt(), coordinates.last().toInt()) }
        }

    val part1Vents = ventPoints.filter { it.first().first == it.last().first || it.first().second == it.last().second }
    println("The number of overlapping vents for Part 1 is: ${overlappingPointCount(part1Vents)}")
    println("The number of overlapping vents for Part 2 is: ${overlappingPointCount(ventPoints)}")
}

fun overlappingPointCount(coordinateLists: List<List<Pair<Int, Int>>>): Int =
    coordinateLists.flatMap {
        val xDiff = it.last().first - it.first().first
        val yDiff = it.last().second - it.first().second
        val direction = Pair(
            when {
                xDiff > 0 -> 1
                xDiff < 0 -> -1
                else      -> 0
            },
            when {
                yDiff > 0 -> 1
                yDiff < 0 -> -1
                else      -> 0
            }
        )
        generateSequence(it.first()) { point -> Pair(point.first + direction.first, point.second + direction.second) }
            .take(max(abs(xDiff), abs(yDiff)) + 1).toList()
    }
        .groupBy { it }
        .count { it.value.size > 1 }
