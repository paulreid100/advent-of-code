package Solutions.Y2021

import java.util.*

fun main() {
    print("Enter your input: ")
    val scanner = Scanner(System.`in`).useDelimiter("\n")
    val scannedDepths = mutableListOf<Int>()
    while (scanner.hasNext()) {
        scannedDepths.add(scanner.nextInt())
    }
    println("There are ${countDepthIncreases(scannedDepths)} measurements larger than the previous one for Part 1.")
    println("There are ${countDepthIncreases(scannedDepths.windowed(3).map { it.sum() })} measurements larger than the previous one for Part 2.")
}

fun countDepthIncreases(depths: List<Int>) =
    depths
        .windowed(2)
        .count { it[1] > it[0] }