package Solutions.Y2021

import java.util.*

fun main() {
    print("Enter your input: ")
    val scanner = Scanner(System.`in`).useDelimiter("\n")
    val commands = mutableListOf<String>()
    while (scanner.hasNext()) {
        commands.add(scanner.next())
    }

    var horizontalPos = 0
    var part1Depth = 0
    var aim = 0
    var part2Depth = 0
    commands.forEach {
        val (direction, extractedDistance) = it.split(' ')
        val distance = extractedDistance.toInt()
        when (direction) {
            "forward" -> {
                horizontalPos += distance
                part2Depth += aim * distance
            }
            "down" -> {
                part1Depth += distance
                aim += distance
            }
            "up" -> {
                part1Depth -= distance
                aim -= distance
            }
        }
    }
    println("Part 1: ${horizontalPos * part1Depth}")
    println("Part 2: ${horizontalPos * part2Depth}")
}