package Solutions.Y2022

import java.io.File

fun main() {
    val input = File("/home/paul/Development/Projects/Personal/adventofcode/Inputs/2022/Day6.txt").readLines().single()
    println("Part 1: ${decode(input, 4)}")
    println("Part 2: ${decode(input, 14)}")
}

fun decode(input: String, markerLength: Int) =
    input.windowed(markerLength).indexOfFirst { it.toSet().size == markerLength } + markerLength