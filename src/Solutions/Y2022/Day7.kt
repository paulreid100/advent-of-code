package Solutions.Y2022

import java.io.File

fun main() {
    val input = File("/home/paul/Development/Projects/Personal/adventofcode/Inputs/2022/Day7.txt").readLines()
    val commandPattern = Regex("\\$ (\\w+) (.+)?")
    val fileDataPattern = Regex("(\\d+|dir) (.+)")
    val currentDirectory = mutableListOf<String>()
    val directories = mutableMapOf<String, Node>()
    input.forEach {
        var fullPath = currentDirectory.joinToString("/")
        if (commandPattern.matches(it)) {
            val (command, arguments) = commandPattern.find(it)!!.destructured
            when (command) {
                "cd" -> if (arguments != "..") currentDirectory.add(arguments) else currentDirectory.removeLast()
            }
            fullPath = currentDirectory.joinToString("/")
            if (!directories.containsKey(fullPath))
                directories[fullPath] = Node()
        }
        if (fileDataPattern.matches(it)) {
            val (sizeOrDir, name) = fileDataPattern.find(it)!!.destructured
            when (sizeOrDir) {
                "dir" -> {
                    val newNode = Node()
                    directories[fullPath]!!.children["$fullPath/$name"] = newNode
                    directories["$fullPath/$name"] = newNode
                }

                else  -> directories[fullPath]!!.children["$fullPath/$name"] = Node(sizeOrDir.toInt())
            }
        }
    }
    println(
        "The sum of directories less than 100000 is ${
            directories.map { it.value.getSize() }.filter { it <= 100000 }.sum()
        }"
    )

    val remainingSpace = 70_000_000 - directories["/"]!!.getSize()
    val spaceToFree = 30_000_000 - remainingSpace
    println(
        "The smallest directory that could be deleted has a size of ${
            directories.map { it.value.getSize() }.filter { it >= spaceToFree }.min()
        }"
    )
}

data class Node(val size: Int? = null, val children: MutableMap<String, Node> = mutableMapOf()) {
    fun getSize(): Int = size ?: children.values.sumOf { it.getSize() }
}