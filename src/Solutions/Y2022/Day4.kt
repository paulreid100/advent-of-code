package Solutions.Y2022

import java.io.File

fun main() {
    val input = File("/home/paul/Development/Projects/Personal/adventofcode/Inputs/2022/Day4.txt").readLines()
    val sectionsPerPair = input.map { pairAssignments ->
        pairAssignments.split(',')
            .map { sectionRange ->
                (sectionRange.substringBefore('-').toInt()..sectionRange.substringAfter('-').toInt()).toSet()
            }
            .sortedBy { it.size }
    }
    println(
        "There are ${
            sectionsPerPair.count { elfAssignments ->
                elfAssignments.first().minus(elfAssignments.last()).isEmpty()
            }
        } assignment pairs where one range fully contains the other"
    )
    println(
        "There are ${
            sectionsPerPair.count { elfAssignments ->
                elfAssignments.first().intersect(elfAssignments.last()).isNotEmpty()
            }
        } assignment pairs where one range overlaps the other"
    )
}