package Solutions.Y2022

import java.util.*

fun main() {
    print("Enter your input: ")
    val scanner = Scanner(System.`in`).useDelimiter("\n")
    val backpacks = mutableListOf<String>()
    while (scanner.hasNext()) {
        val backpack = scanner.next()
        backpacks.add(backpack)
    }
    val letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
    val part1 = backpacks.sumOf {
        letters.indexOf(
            it.substring(0 until it.length / 2).toSet().intersect(it.substring(it.length / 2 until it.length).toSet())
                .first()
        ) + 1
    }
    println("The sum of priorities is $part1")

    val part2 = backpacks.chunked(3)
        .sumOf { letters.indexOf(it[0].toSet().intersect(it[1].toSet()).intersect(it[2].toSet()).first()) + 1 }
    println("The sum of priorities that corresponds to badges is $part2")
}