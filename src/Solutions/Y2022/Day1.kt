package Solutions.Y2022

import java.util.*

fun main() {
    print("Enter your input: ")
    val scanner = Scanner(System.`in`).useDelimiter("\n")
    val caloriesPerElf = mutableListOf<Int>()
    var currentElfTotal = 0
    while (scanner.hasNext()) {
        val value = (scanner.next())
        if (value.isNullOrEmpty()) {
            caloriesPerElf.add(currentElfTotal)
            currentElfTotal = 0
        } else {
            currentElfTotal += value.toInt()
        }
    }
    caloriesPerElf.add(currentElfTotal)
    println("The most calories an elf is carrying is ${caloriesPerElf.max()}")
    println("The top 3 elves are carrying ${caloriesPerElf.sorted().takeLast(3).sum()} calories")
}