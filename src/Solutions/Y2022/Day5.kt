package Solutions.Y2022

import java.io.File

fun main() {
    val input = File("/home/paul/Development/Projects/Personal/adventofcode/Inputs/2022/Day5.txt").readLines()
    val stackLayout = input.takeWhile { it.isNotEmpty() }.reversed().drop(1)
        .map { row -> row.chunked(4).map { it[1] } }
    val instructions = input.dropWhile { it.isNotEmpty() }.drop(1).map {
        it.split("from").flatMap { instructionChunk ->
            instructionChunk.split("to")
                .map { smallerInstructionChunk ->
                    smallerInstructionChunk.filter { instruction -> instruction.isDigit() }.toInt()
                }
        }
    }
    val numberOfStacks = stackLayout.first().size
    val stacks = Array<ArrayDeque<Char>>(numberOfStacks) { ArrayDeque() }
    stackLayout.forEach { it.forEachIndexed { i, box -> if (box.isLetter()) stacks[i].add(box) } }

    instructions.forEach { processInstructionPart2(stacks, it[0], it[1] - 1, it[2] - 1) }
    println("Top crates: ${stacks.joinToString("") { it.last().toString() }}")
}

fun processInstructionPart1(stacks: Array<ArrayDeque<Char>>, cycles: Int, source: Int, destination: Int) {
    repeat(cycles) {
        stacks[destination].add(stacks[source].removeLast())
    }
}

fun processInstructionPart2(stacks: Array<ArrayDeque<Char>>, cycles: Int, source: Int, destination: Int) {
    stacks[destination].addAll(stacks[source].takeLast(cycles))
    repeat(cycles) { stacks[source].removeLast() }
}