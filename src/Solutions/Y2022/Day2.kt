package Solutions.Y2022

import java.util.*

fun main() {
    print("Enter your input: ")
    val scanner = Scanner(System.`in`).useDelimiter("\n")
    val rounds = mutableListOf<Pair<String, String>>()
    while (scanner.hasNext()) {
        val round = scanner.next().split(' ')
        rounds.add(round[0] to round[1])
    }
    val part1 = rounds.sumOf { (them, you) ->
        val shapeScore = "XYZ".indexOf(you) + 1
        val roundScore = when (them to you) {
            "A" to "X", "B" to "Y", "C" to "Z" -> 3
            "A" to "Y", "B" to "Z", "C" to "X" -> 6
            else                               -> 0
        }
        shapeScore + roundScore
    }
    println("Following the misunderstood strat guide gets you a score of $part1")
    val part2 = rounds.sumOf { (them, outcome) ->
        val shapeToPick = when (them to outcome) {
            "A" to "Y", "B" to "X", "C" to "Z" -> "A"
            "A" to "Z", "B" to "Y", "C" to "X" -> "B"
            "A" to "X", "B" to "Z", "C" to "Y" -> "C"
            else                               -> throw Exception("Unknown pairing")
        }
        val shapeScore = "ABC".indexOf(shapeToPick) + 1
        val roundScore = "XYZ".indexOf(outcome) * 3
        shapeScore + roundScore
    }
    println("Following the correct strat guide gets you a score of $part2")
}