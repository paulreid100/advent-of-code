package Solutions.Y2016.Day1

import java.awt.Point
import java.util.regex.Pattern

var facingDirectionDegrees = 0
var currentLocation = Point(0, 0)
var visitedPoints = mutableListOf<Point>()
var firstPreviouslyVisitedPoint: Point? = null

fun main(args: Array<String>) {
    print("Enter your input: ")
    val directions = readLine() ?: ""
    val pattern = Pattern.compile("([LR])(\\d+)")
    for (direction in directions.split(',')) {
        val matcher = pattern.matcher(direction)
        matcher.find()
        facingDirectionDegrees += if (matcher.group(1) == "R") 90 else -90
        val distance = matcher.group(2).toInt()
        when (facingDirectionDegrees % 360) {
            0 -> {
                goNorth(distance)
            }
            90, -270 -> {
                goEast(distance)
            }
            180, -180 -> {
                goSouth(distance)
            }
            270, -90 -> {
                goWest(distance)
            }
        }
    }
    println("Distance travelled is ${Math.abs(currentLocation.x) + Math.abs(currentLocation.y)}")
    println(
        "Distance to first point expanded multiple times is: ${Math.abs(firstPreviouslyVisitedPoint!!.x) + Math.abs(
            firstPreviouslyVisitedPoint!!.y
        )} ($firstPreviouslyVisitedPoint)"
    )
}

fun checkVisitedPoints() {
    if (firstPreviouslyVisitedPoint == null) {
        if (visitedPoints.contains(currentLocation)) {
            firstPreviouslyVisitedPoint = Point(currentLocation)
        } else {
            visitedPoints.add(Point(currentLocation))
        }
    }
}

private fun goNorth(distance: Int) {
    for (i in 1..distance) {
        currentLocation.move(currentLocation.x, ++currentLocation.y)
        checkVisitedPoints()
    }
}

private fun goEast(distance: Int) {
    for (i in 1..distance) {
        currentLocation.move(++currentLocation.x, currentLocation.y)
        checkVisitedPoints()
    }
}

private fun goSouth(distance: Int) {
    for (i in 1..distance) {
        currentLocation.move(currentLocation.x, --currentLocation.y)
        checkVisitedPoints()
    }
}

private fun goWest(distance: Int) {
    for (i in 1..distance) {
        currentLocation.move(--currentLocation.x, currentLocation.y)
        checkVisitedPoints()
    }
}