package Solutions.Y2016.Day8

import java.util.*
import java.util.regex.Pattern

var pixelsOn = 0

fun Array<BooleanArray>.copy() = Array(size) { get(it).clone() }

fun main(args: Array<String>) {
    //Press enter after pasting the input and then press ctrl-D on Linux or ctrl-Z on Windows to signify EOF
    print("Enter your input: ")
    val scanner = Scanner(System.`in`).useDelimiter("\n")
    val pixelGrid = Array(6) { BooleanArray(50) }

    while (scanner.hasNext()) {
        val inputLine = scanner.next()
        val pattern = Pattern.compile("^(rect|rotate) ((\\d+)x(\\d+)|(column|row) ([xy])=(\\d+) by (\\d+))$")
        val matcher = pattern.matcher(inputLine)
        matcher.find()
        val action = matcher.group(1)
        val rectangleWidth = (matcher.group(3) ?: "0").toInt()
        val rectangleHeight = (matcher.group(4) ?: "0").toInt()
        val toBeRotated = matcher.group(5) ?: ""
        val rotateAt = (matcher.group(7) ?: "0").toInt()
        val rotateBy = (matcher.group(8) ?: "0").toInt()

        when (action) {
            "rect" -> {
                createRectangle(rectangleWidth, rectangleHeight, pixelGrid)
            }
            "rotate" -> {
                rotate(toBeRotated, rotateAt, rotateBy, pixelGrid)
            }
        }

    }
    println("Number of pixels on is $pixelsOn")
    printGrid(pixelGrid)
}

fun rotate(toBeRotated: String, rotateAt: Int, rotateBy: Int, pixelGrid: Array<BooleanArray>) {
    when (toBeRotated) {
        "row" -> {
            for (i in 1..rotateBy) {
                val currentState = pixelGrid.copy()
                for (x in 0 until pixelGrid[rotateAt].size) {
                    if (x == 0) {
                        pixelGrid[rotateAt][x] = currentState[rotateAt][pixelGrid[rotateAt].size - 1]
                    } else {
                        pixelGrid[rotateAt][x] = currentState[rotateAt][x - 1]
                    }

                }
            }
        }
        "column" -> {
            for (i in 1..rotateBy) {
                val currentState = pixelGrid.copy()
                for (y in 0 until pixelGrid.size) {
                    if (y == 0) {
                        pixelGrid[y][rotateAt] = currentState[pixelGrid.size - 1][rotateAt]
                    } else {
                        pixelGrid[y][rotateAt] = currentState[y - 1][rotateAt]
                    }
                }
            }
        }
    }
}

fun createRectangle(rectangleWidth: Int, rectangleHeight: Int, pixelGrid: Array<BooleanArray>) {
    for (y in 0 until rectangleHeight) {
        for (x in 0 until rectangleWidth) {
            if (!pixelGrid[y][x]) {
                pixelGrid[y][x] = true
                pixelsOn++
            }
        }
    }
}

fun printGrid(pixelGrid: Array<BooleanArray>) {
    for (y in 0 until pixelGrid.size) {
        for (x in 0 until pixelGrid[y].size) {
            print(if (pixelGrid[y][x]) "1" else " ")
            if (x == pixelGrid[y].size - 1) {
                print("\n")
            }
        }
    }
}
