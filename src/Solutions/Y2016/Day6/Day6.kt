package Solutions.Y2016.Day6

import java.util.*

fun main(args: Array<String>) {
    print("Enter your input: ")
    val scanner = Scanner(System.`in`).useDelimiter("\n")
    val rowList = mutableListOf<CharArray>()
    val message = StringBuilder()
    val messagePart2 = StringBuilder()
    while (scanner.hasNext()) {
        val row = scanner.next()
        rowList.add(row.toCharArray())
    }

    for (i in 0 until rowList[0].size) {
        val column = StringBuilder()
        for (row in rowList) {
            column.append(row[i])
        }
        val columnData = column.toString()
            .groupBy { it }
            .toList()
            .sortedByDescending { it.second.size }
        message.append(columnData.first().first)
        messagePart2.append(columnData.last().first)
    }

    println("The message for Part 1 reads: $message")
    println("The message for Part 2 reads: $messagePart2")
}
