package Solutions.Y2016.Day4

import java.util.*
import java.util.regex.Pattern

fun main(args: Array<String>) {
    //Press enter after pasting the input and then press ctrl-D on Linux or ctrl-Z on Windows to signify EOF
    print("Enter your input: ")
    val scanner = Scanner(System.`in`).useDelimiter("\n")
    val pattern = Pattern.compile("((-?\\w+)+)-(\\d+)\\[(\\w+)]")
    var sectorIdSum = 0
    while (scanner.hasNext()) {
        val potentialRealRoom = scanner.next()
        val matcher = pattern.matcher(potentialRealRoom)
        matcher.find()
        val encryptedName = matcher.group(1)
        val sectorId = matcher.group(3)
        val checksum = matcher.group(4)
        if (isARealRoom(encryptedName, checksum)) {
            sectorIdSum += sectorId.toInt()
            println("${decryptRoomName(encryptedName, sectorId.toInt())} - $sectorId")
        }
    }
    println("The sum of sector IDs is $sectorIdSum")
}

fun decryptRoomName(encryptedName: String, numberOfRotations: Int): String {
    val decryptedName = StringBuilder()
    for (letter in encryptedName) {
        var currentLetter = letter
        for (i in 0..(numberOfRotations % 26 - 1)) {
            when (currentLetter) {
                'z' -> {
                    currentLetter = 'a'
                }
                '-' -> {
                    currentLetter = ' '
                }
                ' ' -> {
                    currentLetter = ' '
                }
                else -> {
                    currentLetter++
                }
            }
        }
        decryptedName.append(currentLetter)
    }
    return decryptedName.toString()
}

fun isARealRoom(encryptedName: String, checksum: String): Boolean {
    val calculatedChecksum = encryptedName
        .filter { it != '-' }
        .groupBy { it }
        .toSortedMap()
        .toList()
        .sortedByDescending { it.second.size }
        .take(5)
        .map { it.first }
        .joinToString("")

    return calculatedChecksum == checksum
}
