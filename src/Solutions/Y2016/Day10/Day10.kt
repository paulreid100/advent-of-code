package Solutions.Y2016.Day10

import java.util.*
import java.util.regex.Pattern

fun main(args: Array<String>) {
    print("Enter your input: ")
    val scanner = Scanner(System.`in`).useDelimiter("\n")
    val pattern =
        Pattern.compile("(value|bot) (\\d+) (goes to bot|gives low to (bot|output)) (\\d+)( and high to (bot|output) (\\d+))?")
    val botList = mutableMapOf<String, BotProps>()
    val instructionList = mutableListOf<String>()

    while (scanner.hasNext()) {
        instructionList.add(scanner.next())
    }
    for (instruction in instructionList.sortedDescending()) {
        val ruleMatcher = pattern.matcher(instruction)
        ruleMatcher.find()
        val ruleType = ruleMatcher.group(1)
        when (ruleType) {
            "value" -> {
                val valueReceived = ruleMatcher.group(2).toInt()
                val receivedByBot = "b${ruleMatcher.group(5)}"
                botReceives(botList, receivedByBot, valueReceived)

            }
            "bot" -> {
                val botToUpdate = "b${ruleMatcher.group(2)}"
                val typeToGiveHighTo = ruleMatcher.group(7)[0]
                val typeToGiveLowTo = ruleMatcher.group(4)[0]
                val idToGiveLowTo = "$typeToGiveLowTo${ruleMatcher.group(5)}"
                val idToGiveHighTo = "$typeToGiveHighTo${ruleMatcher.group(8)}"

                if (botList.contains(botToUpdate)) {
                    val paramsToChange = botList[botToUpdate]
                    paramsToChange!!.giveLowTo = idToGiveLowTo
                    paramsToChange.giveHighTo = idToGiveHighTo
                } else {
                    botList[botToUpdate] = BotProps(giveHighTo = idToGiveHighTo, giveLowTo = idToGiveLowTo)
                }
            }
        }
    }
    propagateValues(botList)
    println("Bots that compare 61 and 17 chips have the following IDs ${botList.filter { it.value.high == 61 && it.value.low == 17 }.keys}")
    println(
        "Multiplying the values of outputs 0, 1, and 2 gives: ${botList.filter { it.key == "o0" || it.key == "o1" || it.key == "o2" }.values.fold(
            1
        ) { x, (low) -> x * low!! }}"
    )
}

tailrec fun propagateValues(botList: MutableMap<String, BotProps>) {
    val readyToProcess = botList
        .filter { it.key.contains('b') && it.value.high != null && it.value.low != null && !it.value.processed }
    if (readyToProcess.isEmpty()) {
        return
    }
    readyToProcess.forEach {
        botReceives(botList, it.value.giveHighTo, it.value.high!!)
        botReceives(botList, it.value.giveLowTo, it.value.low!!)
        it.value.processed = true
    }
    propagateValues(botList)
}

fun botReceives(botList: MutableMap<String, BotProps>, receiverId: String, value: Int) {
    if (botList.contains(receiverId)) {
        val propsToChange = botList[receiverId]
        if (propsToChange!!.low == null) {
            propsToChange.low = value
            return
        }
        if (value < propsToChange.low!!) {
            if (propsToChange.low!! > propsToChange.high ?: Int.MIN_VALUE) {
                propsToChange.high = propsToChange.low
            }
            propsToChange.low = value
        } else {
            if (value > propsToChange.high ?: Int.MIN_VALUE) {
                propsToChange.high = value
            }
        }
    } else {
        botList[receiverId] = BotProps(low = value)
    }
}

data class BotProps(
    var low: Int? = null,
    var high: Int? = null,
    var giveHighTo: String = "default",
    var giveLowTo: String = "default",
    var processed: Boolean = false
)