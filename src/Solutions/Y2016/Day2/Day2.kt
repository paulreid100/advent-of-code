package Solutions.Y2016.Day2

import java.util.*


fun main(args: Array<String>) {
    //Press enter after pasting the input and then press ctrl-D on Linux or ctrl-Z on Windows to signify EOF
    print("Enter your input: ")
    val scanner = Scanner(System.`in`).useDelimiter("\n")

    var row = 1
    var column = 1
    val row0 = arrayOf('1', '2', '3')
    val row1 = arrayOf('4', '5', '6')
    val row2 = arrayOf('7', '8', '9')
    val keypad = arrayOf(row0, row1, row2)
    val resultPart1 = StringBuilder()

    var part2Row = 2
    var part2Column = 0
    val part2Row0 = arrayOf('-', '-', '1', '-', '-')
    val part2Row1 = arrayOf('-', '2', '3', '4', '-')
    val part2Row2 = arrayOf('5', '6', '7', '8', '9')
    val part2Row3 = arrayOf('-', 'A', 'B', 'C', '-')
    val part2Row4 = arrayOf('-', '-', 'D', '-', '-')
    val part2Keypad = arrayOf(part2Row0, part2Row1, part2Row2, part2Row3, part2Row4)
    val resultPart2 = StringBuilder()


    while (scanner.hasNext()) {
        val inputRow = scanner.next()
        for (command in inputRow) {
            when (command) {
                'U' -> {
                    row = goUp(keypad, row, column)
                    part2Row = goUp(part2Keypad, part2Row, part2Column)
                }
                'D' -> {
                    row = goDown(keypad, row, column)
                    part2Row = goDown(part2Keypad, part2Row, part2Column)
                }
                'R' -> {
                    column = goRight(keypad, row, column)
                    part2Column = goRight(part2Keypad, part2Row, part2Column)
                }
                'L' -> {
                    column = goLeft(keypad, row, column)
                    part2Column = goLeft(part2Keypad, part2Row, part2Column)
                }
            }
        }
        resultPart1.append(keypad[row][column])
        resultPart2.append(part2Keypad[part2Row][part2Column])
    }
    println("The keycode for Part 1 is: $resultPart1")
    println("The keycode for Part 2 is: $resultPart2")
}

fun goLeft(keypad: Array<Array<Char>>, row: Int, column: Int): Int {
    return if (column - 1 < 0 || keypad[row][column - 1] == '-') {
        column
    } else {
        column - 1
    }
}

fun goRight(keypad: Array<Array<Char>>, row: Int, column: Int): Int {
    return if (column + 1 >= keypad[row].size || keypad[row][column + 1] == '-') {
        column
    } else {
        column + 1
    }
}

fun goDown(keypad: Array<Array<Char>>, row: Int, column: Int): Int {
    return if (row + 1 >= keypad.size || keypad[row + 1][column] == '-') {
        row
    } else {
        row + 1
    }
}

fun goUp(keypad: Array<Array<Char>>, row: Int, column: Int): Int {
    return if (row - 1 < 0 || keypad[row - 1][column] == '-') {
        row
    } else {
        row - 1
    }
}
