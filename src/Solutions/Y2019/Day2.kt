package Solutions.Y2019

const val part2DesiredValue = 19690720

fun main() {
    print("Enter your input: ")
    val intCodesOriginal = readLine()!!.split(',').map { it.toInt() }
    val intCodesPart1 = intCodesOriginal.toMutableList().setNounAndVerb(12, 2)
    executeProgram(intCodesPart1)
    println(intCodesPart1[0])

    for (noun in 0..99) {
        for (verb in 0..99) {
            val intCodes = intCodesOriginal.toMutableList().setNounAndVerb(noun, verb)
            executeProgram(intCodes)
            if (intCodes[0] == part2DesiredValue) {
                println("100 * $noun + $verb = ${100 * noun + verb}")
                return
            }
        }
    }
}

fun mapFunctionalOpcode(opcode: Int): (Int, Int) -> Int {
    return when (opcode) {
        1 -> Int::plus
        2 -> Int::times
        else -> { _: Int, _: Int -> -999 }
    }
}

fun executeProgram(intCodes: MutableList<Int>) {
    for (position in intCodes.indices step 4) {
        if (intCodes[position] == 99) return
        intCodes[intCodes[position + 3]] = mapFunctionalOpcode(intCodes[position])(intCodes[intCodes[position + 1]], intCodes[intCodes[position + 2]])
    }
}

fun MutableList<Int>.setNounAndVerb(noun: Int, verb: Int): MutableList<Int> {
    this[1] = noun
    this[2] = verb
    return this
}