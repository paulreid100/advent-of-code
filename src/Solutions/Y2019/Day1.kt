package Solutions.Y2019

import java.util.*
import kotlin.math.floor

fun main() {
    print("Enter your input: ")
    val scanner = Scanner(System.`in`).useDelimiter("\n")
    val moduleMasses = mutableListOf<Double>()
    while (scanner.hasNext()) {
        moduleMasses.add(scanner.next().toDouble())
    }
    println("The sum of the fuel requirements is ${moduleMasses.map { calculateFuelForMass(it) }.sum()}")
    println("The sum of the fuel requirements when taking additional fuel into account is ${moduleMasses.map { calculateTotalFuelForMass(it) }.sum()}")

}

fun calculateTotalFuelForMass(mass: Double): Double {
    return if (mass < 9)
        0.0
    else {
        val extraFuelForFuel = calculateFuelForMass(mass)
        extraFuelForFuel + calculateTotalFuelForMass(extraFuelForFuel)
    }
}

fun calculateFuelForMass(mass: Double) = floor(mass / 3) - 2