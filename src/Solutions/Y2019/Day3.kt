package Solutions.Y2019

import java.awt.Point
import java.util.*
import kotlin.math.abs

fun main() {
    print("Enter your input: ")
    val scanner = Scanner(System.`in`).useDelimiter("\n")
    val wireRoutes = mutableListOf<List<String>>()
    while (scanner.hasNext()) {
        wireRoutes.add(scanner.next().split(','))
    }
    val wirePositions = wireRoutes.map { path ->
        val pointPath = mutableListOf(Point(0, 0))
        path.forEach { pointPath.addAll(getNextSequenceInPath(pointPath.last(), it)) }
        pointPath
    }
    val intersections = wirePositions[0].intersect(wirePositions[1]).filter { it != Point(0, 0) }
    val minDistance = intersections
        .map { abs(it.x) + abs(it.y) }
        .minOrNull()
    println("The distance to the closest intersection is $minDistance")

    val minStepsToIntersection = intersections
        .map { wirePositions[0].indexOf(it) + wirePositions[1].indexOf(it) }
        .minOrNull()
    println("Fewest steps required to reach intersection is $minStepsToIntersection")
}

fun getNextSequenceInPath(currentPosition: Point, instruction: String): Sequence<Point> {
    val newPosition = Point(currentPosition)
    val distance = instruction.substring(1).toInt()
    return when (instruction[0]) {
        'U' -> generateSequence(newPosition.apply { y += 1 }, { Point(it.x, it.y + 1) }).take(distance)
        'R' -> generateSequence(newPosition.apply { x += 1 }, { Point(it.x + 1, it.y) }).take(distance)
        'D' -> generateSequence(newPosition.apply { y -= 1 }, { Point(it.x, it.y - 1) }).take(distance)
        'L' -> generateSequence(newPosition.apply { x -= 1 }, { Point(it.x - 1, it.y) }).take(distance)
        else -> emptySequence()
    }
}