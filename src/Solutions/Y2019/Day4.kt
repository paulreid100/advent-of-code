package Solutions.Y2019

import java.util.regex.Pattern
import java.util.stream.Collectors.toList

val range = 240298..784956

fun main() {
    println(range.sumOf{ if (meetsCriteria(it)) 1 as Int else 0 })
    println(range.sumOf{ if (meetsCriteria(it, true)) 1 as Int else 0 })
}

fun meetsCriteria(value: Int, isPart2: Boolean = false): Boolean {
    var digitsIncreasing = true
    var doubleIsNotPartOfLargerGroup = true
    val valueAsString = value.toString()
    val valueAsIntCollection = valueAsString.map { it.code }
    val doubleDigitPattern = Pattern.compile("(\\d)\\1")
    val doubleDigitMatcher = doubleDigitPattern.matcher(valueAsString)
    val tripleDigitPattern = Pattern.compile("(\\d)\\1\\1")
    val tripleDigitMatcher = tripleDigitPattern.matcher(valueAsString)
    valueAsIntCollection.forEachIndexed { index, element ->
        if (index != 5 && digitsIncreasing)
            digitsIncreasing = valueAsIntCollection[index + 1] >= element
    }
    val doubleResults = doubleDigitMatcher.results().collect(toList())
    if (isPart2) {
        val tripleResults = tripleDigitMatcher.results().collect(toList())
        if (tripleResults.any()) {
            doubleIsNotPartOfLargerGroup = false
            if (doubleResults.map { valueAsString[it.start()] }.distinct().size > tripleResults.map { valueAsString[it.start()] }.distinct().size)
                doubleIsNotPartOfLargerGroup = true
        } else
            doubleIsNotPartOfLargerGroup = true
    }
    return doubleResults.any() && digitsIncreasing && doubleIsNotPartOfLargerGroup
}